/*
 * main.c
 */

#include <stdbool.h>
#include <stdint.h>

#include "../TivaWare/inc/hw_adc.h"
#include "../TivaWare/inc/hw_ints.h"
#include "../TivaWare/inc/hw_memmap.h"
#include "../TivaWare/inc/hw_types.h"
#include "../TivaWare/driverlib/sysctl.h"
#include "../TivaWare/driverlib/gpio.h"
#include "../TivaWare/driverlib/adc.h"
#include "../TivaWare/driverlib/pin_map.h"
#include "../TivaWare/driverlib/udma.h"
#include "../TivaWare/driverlib/pwm.h"
#include "../TivaWare/driverlib/rom_map.h"
#include "../TivaWare/driverlib/timer.h"
#include "../TivaWare/driverlib/interrupt.h"

#include "arm_math.h"

#include "Analog.h"
#include "GainPot.h"
#include "uartDebug.h"

#define SAMPLES 512
#define FFT_SIZE SAMPLES / 2

const uint32_t BUCKET_WIDTH = SAMPLE_RATE / SAMPLES;

float32_t Input[SAMPLES];
float32_t Output[SAMPLES*2];
float32_t OutputMag[SAMPLES*2];

float32_t maxVal = 0;
uint32_t maxIndex;
uint32_t scaleVal;


arm_rfft_fast_instance_f32 S;

#define FREQ_BKTS	32
//#define NUM_LEDS	16
#define NUM_LEDS	32
#define LED_CLK		2
#define LED_DAT		1
#define LED_LATCH	4
#define LED_OE		8

bool decayFlag  = false;
uint32_t debugCount = 0;

uint32_t ledBrightness[FREQ_BKTS];

// Light decay timer interrupt
extern "C"{
extern void IntTimerDecay(void);
}



// Frequency limits -----------------------------------------------
// Limits calculated by using an equal number of notes for each bucket (besides the ends)
// Equation for finding frequencies:
// f = f0 * a^n
// f0 is base note frequency (440 for a4)
// a = 2^(1/12) | from 12 half steps
// n = number of notes away from f0

// 7kHz max frequency
//const float32_t freqLimit[16] = {61.73541266,87.30705786,123.4708253,174.6141157,246.9416506,349.2282314,493.8833013,698.4564629,987.7666025,1396.912926,1975.533205,2793.825851,3951.06641,5587.651703,7902.13282,7902.13282};
//const float32_t freqLimit[32] = {61.73541266,72.36363862,84.8215954,99.42428522,116.5409404,136.6043593,160.1218501,187.6880578,220,257.8746915,302.2698024,354.307873,415.3046976,486.8025945,570.609404,668.8442003,783.990872,918.9609285,1077.167118,1262.60972,1479.977691,1734.76723,2033.420746,2383.489763,2793.825851,3274.804453,3838.587219,4499.429523,5274.040911,6182.007605,7246.287746,8493.791897};

//5kHz max frequency
//const float32_t freqLimit[16] = {61.73541266,82.40688923,110,146.832384,195.997718,261.6255653,349.2282314,466.1637615,622.2539674,830.6093952,1108.730524,1479.977691,1975.533205,2637.020455,3520,4698.636287};
const float32_t freqLimit[32] = {61.73541266,71.32617551,82.40688923,95.20902171,110,127.0887967,146.832384,169.6431908,195.997718,226.4464921,261.6255653,302.2698024,349.2282314,403.481779,466.1637615,538.5835591,622.2539674,718.9227994,830.6093952,959.6468047,1108.730524,1280.974801,1479.977691,1709.896216,1975.533205,2282.437616,2637.020455,3046.688695,3520,4066.841493,4698.636287,5428.582105};

// Mel filter bank Weights come from pyfilterbank
const float32_t freqWeights[32] = {0.040277264,0.052938302,0.068532583,0.08741384,0.109892259,0.136226134,0.166624217,0.201253309,0.240240875,0.283661884,0.331502912,0.383603073,0.439579154,0.49875076,0.560089358,0.62221937,0.683494142,0.742150006,0.796511189,0.845191215,0.887230832,0.92213514,0.949810801,0.970435922,0.984306299,0.991693803,0.992737112,0.987372009,0.975302688,0.956016388,0.928847861,0.893103128};


void updateLeds(uint32_t* brightnessArray, uint16_t size)
{
	// Brightness is 10-byte, 100% = 1024, 0% = 0
	uint32_t timerVal = TimerValueGet(TIMER1_BASE, TIMER_A);
	uint32_t timerMax = TimerLoadGet(TIMER1_BASE, TIMER_A);
	uint32_t scale = timerMax >> 10;
	uint8_t status = 0;
	uint16_t i = 0;

	uint32_t max = 0;

	// GPIO debug
	/*static uint8_t gpioVal = 0xff;
	gpioVal = ~gpioVal;
	GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3, gpioVal);*/

	for(i = 0; i < size; i++){
		if(brightnessArray[i] > max) max = brightnessArray[i];
		status = (brightnessArray[i] * scale > timerVal) ? LED_DAT : 0;
		GPIOPinWrite(GPIO_PORTD_BASE, LED_DAT , status);
		GPIOPinWrite(GPIO_PORTD_BASE, LED_CLK , LED_CLK);
		GPIOPinWrite(GPIO_PORTD_BASE, LED_CLK , 0);
	}
	GPIOPinWrite(GPIO_PORTD_BASE, LED_LATCH , LED_LATCH);
	GPIOPinWrite(GPIO_PORTD_BASE, LED_LATCH , 0);

	//debug.Printf("Max: %4d\n",max);
}

void testPattern(int num){
	uint32_t brightness[NUM_LEDS] = {0};
	int single = 0;
	switch(num){
	case 0:
		for(int i = 0; i < NUM_LEDS; i++){
			brightness[i] = 0;
		}
		break;
	case 1:
		for(int i = 0; i < NUM_LEDS; i++){
			brightness[i] = 1024;
		}
		break;
	case 2:
		for(int i = 0; i < NUM_LEDS; i++){
			single += 100;
			brightness[i] = single % 1024;
		}
		break;
	}

	updateLeds(brightness, NUM_LEDS);
}

void LedCheck(){
	// Turn on all LEDs
	uint32_t fullOn[NUM_LEDS];
	for(int i = 0; i < NUM_LEDS; i++){
		fullOn[i] = 1024;
	}
	updateLeds(fullOn, NUM_LEDS);
}

void BrightnessCheck(int brightness){
	// Set all leds, 0-1024
	uint32_t status[NUM_LEDS];
	for(int i = 0; i < NUM_LEDS; i++){
		status[i] = brightness;
	}
	updateLeds(status, NUM_LEDS);
}

void OutputToBuckets(float32_t* input, float32_t* output, uint32_t bucketLen){
	for(int i = 0; i < FREQ_BKTS; i++){
		output[i] = 0;
	}

	// Test using calculated buckets
	int j = 1;
	for(int i = 0; i < FREQ_BKTS; i++){
		do{
			if(input[j] > output[i])
				output[i] = input[j];
			j++;
		}while((j * BUCKET_WIDTH < freqLimit[i]) && (j < SAMPLES));
	}

	// Pack the rest of the samples into the top bucket
	for(int i = j; i < SAMPLES; i++){
		if(input[i] > output[FREQ_BKTS-1])
			output[FREQ_BKTS-1] = input[i];
	}
}


void SetDecayFlag(){
	TimerIntClear(TIMER2_BASE, TIMER_TIMA_TIMEOUT);
	decayFlag = true;
}

void IntTimerDecay(void){SetDecayFlag();}

bool CheckDecayFlag(){
	bool result = decayFlag;
	decayFlag = false;
	return result;
}

int main(void) {

	volatile int max1, max2, max3;
	float32_t bandMag[FREQ_BKTS] = {0};
	float32_t scaledBuckets[FREQ_BKTS] = {0};
	float32_t middleBuckets[FREQ_BKTS] = {0};

	bool enableDecay = false;

	// Set up 50MHz clock
	SysCtlClockSet(SYSCTL_SYSDIV_4 | SYSCTL_USE_PLL | SYSCTL_XTAL_16MHZ | SYSCTL_OSC_MAIN);
	SysCtlADCSpeedSet(SYSCTL_ADCSPEED_500KSPS);
	SysCtlDelay(10);

	// RGB LED Pins
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
	GPIOPinTypeGPIOOutput(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3);
	GPIODirModeSet(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, GPIO_DIR_MODE_OUT);
	GPIOPadConfigSet(GPIO_PORTF_BASE, GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);
	SysCtlDelay(10);

	// Initialize debug uart
	debug.Init();

	// Shift Register Pins
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOD);
	GPIOPinTypeGPIOOutput(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1|GPIO_PIN_2);
	GPIODirModeSet(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1|GPIO_PIN_2, GPIO_DIR_MODE_OUT);
	GPIOPadConfigSet(GPIO_PORTD_BASE, GPIO_PIN_0 | GPIO_PIN_1|GPIO_PIN_2, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD);

	// Set up pwm timer
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER1);
	SysCtlDelay(10);
	TimerConfigure(TIMER1_BASE, TIMER_CFG_PERIODIC);
	uint32_t TimerLoadTime = SysCtlClockGet() / 120; // 120Hz update rate
	TimerLoadSet(TIMER1_BASE, TIMER_A, TimerLoadTime);
	TimerEnable(TIMER1_BASE, TIMER_A);

	// Set up decay timer
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER2);
	TimerLoadTime = SysCtlClockGet() / 1000;
	TimerLoadSet(TIMER2_BASE, TIMER_A, TimerLoadTime);
	TimerEnable(TIMER2_BASE, TIMER_A);
	IntEnable(INT_TIMER2A);
	TimerIntEnable(TIMER2_BASE, TIMER_TIMA_TIMEOUT);

	TimerEnable(TIMER2_BASE, TIMER_A);

	// Set up analog sampling for audio
	Analog analog;
	analog.Init();
	analog.SetSampleSize(SAMPLES);

	for(int i = 0; i < FREQ_BKTS; i++){
		scaledBuckets[i] = 0;
		ledBrightness[i] = 0;
	}

	analog.StartSampling();

	for(;;){

		/*for(;;){
			BrightnessCheck(1000);
			//testPattern(2);
		}*/

		if(analog.SamplesReady()){

			// Loop timing debug
			// static uint8_t gpioVal = 0xff;
			// gpioVal = ~gpioVal;
			// GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_3, gpioVal);*/

			for(int i = 0; i < SAMPLES; i++){
				Input[i] = float32_t(analog.GetSamples(i)) / 4096.0;
			}

			// FFT on samples
			arm_rfft_fast_init_f32(&S, SAMPLES);
			arm_rfft_fast_f32(&S, Input, Output,0);
			arm_cmplx_mag_f32(Output, OutputMag, SAMPLES*2);

			// Update LED PWM
			OutputMag[0] = 0;	// Remove dc component

			OutputToBuckets(OutputMag, bandMag, SAMPLES);

			// Use timer to change rate of decay
			enableDecay = CheckDecayFlag();
			for(int i = 0; i < FREQ_BKTS; i++){
				if(bandMag[i] > middleBuckets[i])
					middleBuckets[i] = bandMag[i];
				else if(enableDecay && middleBuckets[i] > 0){
					middleBuckets[i]--;
				}
			}

			// Increase weight as frequency increases
			for(int i = 0; i < FREQ_BKTS; i++){
				scaledBuckets[i] = middleBuckets[i]*middleBuckets[i]*freqWeights[i]*10; // Only for 32 currently
			}

			// Restart sampling
			analog.StartSampling();
		}

		// Compress output and limit if too high
		for(int i = 0; i < FREQ_BKTS; i++){
			scaledBuckets[i] = scaledBuckets[i];
			if(scaledBuckets[i] > 900)
				scaledBuckets[i] = 900 + (scaledBuckets[i] - 900) / 4;

			if(scaledBuckets[i] > 1024)
				scaledBuckets[i] = 1024;

			if(scaledBuckets[i] > maxVal)
				maxVal = scaledBuckets[i];
		}

		for(int i = 0; i < FREQ_BKTS; i++){
			ledBrightness[i] = ((uint32_t)(scaledBuckets[i] ) ) ;
		}

		updateLeds(ledBrightness, NUM_LEDS);
	}
	//return 0;
}
