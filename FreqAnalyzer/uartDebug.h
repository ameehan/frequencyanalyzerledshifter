/*
 * uartDebug.h
 *
 *  Created on: Jul 24, 2015
 *      Author: ameehan
 */

#ifndef UARTDEBUG_H_
#define UARTDEBUG_H_

#include <stdarg.h>

class DEBUG{
public:
	void Init();
	void PrintChar(char temp);
	void Print(char *temp);
	void Printf(const char *pcString, ...);
};

extern DEBUG debug;

#endif /* UARTDEBUG_H_ */
