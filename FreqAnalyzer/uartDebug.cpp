/*
 * uartDebug.cpp
 *
 *  Created on: Jul 24, 2015
 *      Author: ameehan
 */


#include "uartDebug.h"

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#include "../TivaWare/driverlib/sysctl.h"
#include "../TivaWare/inc/hw_memmap.h"
#include "../TivaWare/inc/hw_types.h"
#include "../TivaWare/driverlib/gpio.h"
#include "../TivaWare/driverlib/pin_map.h"
#include "../TivaWare/driverlib/uart.h"
#include "uartstdio.h"

#define tidDebug


DEBUG debug;

void DEBUG::Init()
{
	SysCtlPeripheralEnable(SYSCTL_PERIPH_UART0);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOA);
	GPIOPinConfigure(GPIO_PA0_U0RX);
	GPIOPinConfigure(GPIO_PA1_U0TX);
	GPIOPinTypeUART(GPIO_PORTA_BASE, GPIO_PIN_0 | GPIO_PIN_1);

	UARTClockSourceSet(UART0_BASE, UART_CLOCK_PIOSC);
	UARTStdioConfig(0, 115200, 16000000);
}

void DEBUG::Print(char* temp)
{
	char *x = temp;
	while(*x != NULL){
		UARTCharPut	(UART0_BASE	, *x);
		++x;
		SysCtlDelay(10);
	}

}

void DEBUG::PrintChar(char temp)
{
	UARTCharPut(UART0_BASE, temp);
}

void DEBUG::Printf(const char *pcString, ...)
{
	va_list vaArgP;
	va_start(vaArgP, pcString);
	UARTvprintf(pcString, vaArgP);
}
