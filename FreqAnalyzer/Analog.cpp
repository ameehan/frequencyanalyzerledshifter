/*
 * Analog.cpp
 *
 *  Created on: May 9, 2016
 *      Author: ameehan
 */

#include "Analog.h"

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#include "../TivaWare/driverlib/adc.h"
#include "../TivaWare/inc/hw_adc.h"
#include "../TivaWare/inc/hw_ints.h"
#include "../TivaWare/inc/hw_memmap.h"
#include "../TivaWare/inc/hw_types.h"
#include "../TivaWare/driverlib/sysctl.h"
#include "../TivaWare/driverlib/timer.h"
#include "../TivaWare/driverlib/gpio.h"
#include "../TivaWare/driverlib/interrupt.h"


static Analog* instance = NULL;
volatile uint32_t samples[1024];

extern "C"{
extern void IntAdc0Seq0Handler(void);
}

void IntAdc0Seq0Handler(void){instance->Interrupt();}


Analog::Analog() {
	sampleSize = 0;
	sampleIndex = 0;
	samplesReady = false;
	gpioVal = 0xff;
}

Analog::~Analog() {
}

bool Analog::Init(){

	if( instance != NULL){
		return false;
	}
	instance = this;

	// Initialize the ADC
	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	SysCtlDelay(10);
	ADCReferenceSet(ADC0_BASE, ADC_REF_INT);
	ADCSequenceConfigure(ADC0_BASE, 0, ADC_TRIGGER_TIMER, 0);
	ADCSequenceStepConfigure(ADC0_BASE, 0, 0, ADC_CTL_CH0 | ADC_CTL_IE |
	                             ADC_CTL_END);

	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);

	// Initialize adc timer
	SysCtlPeripheralEnable(SYSCTL_PERIPH_TIMER0);
	TimerConfigure(TIMER0_BASE, TIMER_CFG_A_PERIODIC);
	uint32_t clock = SysCtlClockGet();
	TimerLoadSet(TIMER0_BASE, TIMER_A, clock / SAMPLE_RATE );
	TimerADCEventSet(TIMER0_BASE, TIMER_ADC_TIMEOUT_A);
	TimerControlTrigger(TIMER0_BASE, TIMER_A, true);

	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
	SysCtlDelay(10);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	SysCtlDelay(10);
	GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_3);

	ADCSequenceEnable(ADC0_BASE, 0);
	ADCIntClear(ADC0_BASE, 0);

	return true;
}

void Analog::SetSampleSize(uint32_t size){
	sampleSize = size;
}

uint32_t Analog::GetSample(){
	uint32_t vals[1];
	ADCIntClear(ADC0_BASE, 0);
	ADCProcessorTrigger(ADC0_BASE, 0);
	while(!ADCIntStatus(ADC0_BASE, 0, false))
	{
	}
	ADCSequenceDataGet(ADC0_BASE, 0, vals);
	return vals[0];
}

bool Analog::SamplesReady(){
	return samplesReady;
}

void Analog::StartSampling(){
	samplesReady = false;
	sampleIndex = 0;

	TimerEnable(TIMER0_BASE, TIMER_A);
	ADCIntClear(ADC0_BASE, 0);
	ADCIntEnable(ADC0_BASE,0);

	IntEnable(INT_ADC0SS0);
}

uint32_t Analog::GetSamples(uint32_t num){
	return samples[num];
}

void Analog::Interrupt(){
	// Add new sample to the sample array on every interrupt.
	// Could be improved by dumping all samples to array through uDMA
	uint32_t vals[8];
	ADCIntClear(ADC0_BASE, 0);

	// Debugging
	//static uint8_t tempToggle = 0xff;
	//tempToggle = ~tempToggle;
	//GPIOPinWrite(GPIO_PORTF_BASE, GPIO_PIN_2, tempToggle);

	while(ADCBusy(ADC0_BASE))
	{
	}

	int32_t ret = ADCSequenceDataGet(ADC0_BASE, 0, vals);
	samples[sampleIndex] = vals[0];
	sampleIndex++;

	if(sampleIndex >= sampleSize){
		IntDisable(INT_ADC0SS0);
		ADCIntDisable(ADC0_BASE,0);
		TimerDisable(TIMER0_BASE, TIMER_A);
		samplesReady = true;
	}

}
