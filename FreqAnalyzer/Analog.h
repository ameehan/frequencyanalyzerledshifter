/*
 * Analog.h
 *
 *  Created on: May 9, 2016
 *      Author: ameehan
 */

#ifndef ANALOG_H_
#define ANALOG_H_

#include <stdint.h>

#define SAMPLE_RATE 20000

class Analog {
public:
	Analog();
	virtual ~Analog();
	bool Init();
	uint32_t GetSample();
	void StartSampling();
	void Interrupt();
	bool SamplesReady();
	void SetSampleSize(uint32_t size);
	uint32_t GetSamples(uint32_t num);
	uint8_t gpioVal;

private:
	uint32_t sampleSize;
	volatile uint32_t sampleIndex;
	//uint32_t samples[1024];
	volatile bool samplesReady;
};

#endif /* ANALOG_H_ */
