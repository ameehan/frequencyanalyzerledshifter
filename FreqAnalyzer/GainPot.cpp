/*
 * GainPot.cpp
 *
 *  Created on: Jun 10, 2016
 *      Author: ameehan
 */

#include "GainPot.h"
#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#include "../TivaWare/inc/hw_memmap.h"
#include "../TivaWare/inc/hw_types.h"
#include "../TivaWare/driverlib/adc.h"
#include "../TivaWare/driverlib/sysctl.h"
#include "../TivaWare/driverlib/gpio.h"
#include "../TivaWare/driverlib/pin_map.h"

static GainPot* instance = NULL;

GainPot::GainPot() {
	// TODO Auto-generated constructor stub

}

bool GainPot::Init(){
	if( instance != NULL){
		return false;
	}
	instance = this;

	SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC1);
	SysCtlDelay(10);

	ADCReferenceSet(ADC1_BASE, ADC_REF_EXT_3V);
	ADCSequenceConfigure(ADC1_BASE, 0, ADC_TRIGGER_PROCESSOR, 3);
	ADCSequenceStepConfigure(ADC1_BASE, 0, 0, ADC_CTL_CH1| ADC_CTL_IE |
            ADC_CTL_END);
	ADCSequenceEnable(ADC1_BASE, 0);
	ADCIntClear(ADC1_BASE, 0);
	SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOE);
	SysCtlDelay(10);
	GPIOPinTypeADC(GPIO_PORTE_BASE, GPIO_PIN_2); //ADC1
	return true;
}

uint32_t GainPot::GetVal(){
	uint32_t val = 0;
	ADCIntClear(ADC1_BASE, 0);
	ADCProcessorTrigger(ADC1_BASE, 0);
	while(!ADCIntStatus(ADC1_BASE, 0, false)){}

	ADCSequenceDataGet(ADC1_BASE, 0, &val);
	return val;
}

GainPot::~GainPot() {
	// TODO Auto-generated destructor stub
}

