/*
 * GainPot.h
 *
 *  Created on: Jun 10, 2016
 *      Author: ameehan
 */

#ifndef GAINPOT_H_
#define GAINPOT_H_

#include <stdbool.h>
#include <stdint.h>

class GainPot {
public:
	GainPot();
	virtual ~GainPot();
	bool Init();
	uint32_t GetVal();
};

#endif /* GAINPOT_H_ */
